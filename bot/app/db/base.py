from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import sessionmaker
from db.settings import Settings

engine = create_engine(Settings().uri)

Session = sessionmaker(bind=engine)

Base = declarative_base()
