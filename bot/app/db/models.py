from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, BigInteger, ForeignKey, Boolean
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship, backref

import uuid

from db.base import Base
from datetime import date


class User(Base):
    __tablename__ = "user"

    id = Column(BigInteger, primary_key=True)
    name = Column(String(64))
    username = Column(String(32))

    owns_funds = relationship("Fund", backref="owner", lazy="dynamic")
    fund_members = relationship("FundMember", backref="user", lazy="dynamic")
    group_members = relationship("GroupMember", backref="user", lazy="dynamic")


class Group(Base):
    __tablename__ = "group"

    id = Column(BigInteger, primary_key=True)

    funds = relationship("Fund", backref="group", lazy="dynamic")

    group_members = relationship("GroupMember", backref="group", lazy="dynamic")


class GroupMember(Base):
    __tablename__ = "groupmember"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    user_id = Column(BigInteger, ForeignKey("user.id"))
    group_id = Column(BigInteger, ForeignKey("group.id"))


class Fund(Base):
    __tablename__ = "fund"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(
        String(40), default="Сбор {date}".format(date=date.today().strftime("%d.%m"))
    )
    description = Column(String(120), default=None)
    owner_id = Column(BigInteger, ForeignKey("user.id"))
    group_id = Column(BigInteger, ForeignKey("group.id"))
    amount = Column(Integer)
    active = Column(Boolean, default=True)

    members = relationship("FundMember", backref="fund", lazy="dynamic")


class FundMember(Base):
    __tablename__ = "fundmember"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    user_id = Column(BigInteger, ForeignKey("user.id"))
    fund_id = Column(UUID(as_uuid=True), ForeignKey("fund.id"))
    contributed = Column(Boolean, default=False)
