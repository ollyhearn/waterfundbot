from telebot.types import (
    InlineKeyboardButton as button,
    InlineKeyboardMarkup as keyboard,
)


def setup() -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="💸 Стать участником", callback_data="register_group_member")],
        ]
    )


def cancel() -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="❌ Отменить", callback_data="cancel")],
        ]
    )


def fund_markup() -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="✅ Я скинул", callback_data="contributed")],
            [button(text="🏁 Завершить сбор", callback_data="close_fund")],
        ]
    )


def yes_no() -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="✅ Да", callback_data="yes")],
            [button(text="❌ Нет", callback_data="no")],
        ]
    )
